package com.wolfmanga.products;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wolfmanga.R;
import com.wolfmanga.model.Item;
import com.wolfmanga.util.GlideUtil;

import java.util.ArrayList;
import java.util.List;

public class BannerAdapter extends PagerAdapter {
    private Context mContext;
    private List<Item> itemList;

    public BannerAdapter(List<Item> itemList, Context context) {
        this.itemList = itemList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Item item = itemList.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.pager_item, container, false);
        ImageView imgBanner=itemView.findViewById(R.id.img_banner);
        GlideUtil.loadImageCirculer(mContext,imgBanner,item.getImage().getMobile(),R.drawable.placeholder);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    public void destroyItem(@NonNull ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }

    public void setItemList(List<Item> itemList) {
        this.itemList.addAll(itemList);
        notifyDataSetChanged();
    }
}
