package com.wolfmanga.products;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wolfmanga.R;
import com.wolfmanga.model.Item;
import com.wolfmanga.util.GlideUtil;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private List<Item> itemList;
    private Context context;

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItem,txtCounter,txtTime;
        public ImageView imgItem;


        public ProductViewHolder(View view) {
            super(view);
            imgItem = (ImageView) view.findViewById(R.id.img_item);
            txtItem = (TextView) view.findViewById(R.id.item_content);
            txtCounter = (TextView) view.findViewById(R.id.txt_counter);
            txtTime = (TextView) view.findViewById(R.id.txt_time);
        }
    }


    public ProductAdapter(List<Item> itemList,Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false);

        return new ProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Item item = itemList.get(position);
        holder.txtItem.setText(item.getCategory());
        holder.txtCounter.setText(String.valueOf(item.getItemCount()));
       // holder.txtTime.setText(String.valueOf(item.getItemCount()));
        GlideUtil.loadImageCirculer(context,holder.imgItem,item.getImage().getMobile(),R.drawable.placeholder);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,ProductDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
