package com.wolfmanga.products;

import com.wolfmanga.WolfMangaApp;
import com.wolfmanga.model.ProductDetail;
import com.wolfmanga.util.JsonUtil;

public class ProductPresenter {
    private ProductView view;
    public ProductPresenter(ProductView view) {
        this.view = view;
    }

    public void getProductDetail(){
        String jsonData= JsonUtil.getAssetJsonData(WolfMangaApp.getInstance(),"product_detail.json");
       ProductDetail productDetail= (ProductDetail) JsonUtil.toModel(jsonData,ProductDetail.class);
       view.onProductDetailSuccess(productDetail);
    }


}
