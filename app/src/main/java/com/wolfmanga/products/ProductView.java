package com.wolfmanga.products;

import com.wolfmanga.model.ProductDetail;

public interface ProductView {
    void onProductDetailSuccess(ProductDetail productDetail);
}
