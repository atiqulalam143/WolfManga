package com.wolfmanga.products;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wolfmanga.R;
import com.wolfmanga.model.Item;
import com.wolfmanga.model.ProductDetail;
import com.wolfmanga.util.JsonUtil;

import java.util.ArrayList;

public class ProductDetailActivity extends AppCompatActivity implements ProductView{

    private ViewPager pager;
    private ImageButton btnBack;
    private View layoutBottomSheet;
    private BottomSheetBehavior sheetBehavior;
    private TextView txtPrice,txtName;
    private ProductDetail productDetail;
    private BannerAdapter adapter;
    private ProductPresenter productPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        pager=findViewById(R.id.pager);
        btnBack=findViewById(R.id.btn_back);
        txtName=findViewById(R.id.txt_name);
        txtPrice=findViewById(R.id.txt_price);
        layoutBottomSheet=findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        productPresenter=new ProductPresenter(this);


        adapter=new BannerAdapter(new ArrayList<Item>(),this);
        pager.setAdapter(adapter);
        productPresenter.getProductDetail();
       // pager.setCurrentItem(0);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
       // sheetBehavior.setPeekHeight(50);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                       // btnBottomSheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                       // btnBottomSheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                txtName.setText(productDetail.getData().getItems().get(i).getTitle());
                txtPrice.setText("₹"+productDetail.getData().getItems().get(i).getPrice());
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });


    }

    @Override
    public void onProductDetailSuccess(ProductDetail productDetail) {
        this.productDetail=productDetail;
        adapter.setItemList(productDetail.getData().getItems());
    }
}
