package com.wolfmanga.util;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;

/**
 * This Class Handle JSON to Object and object to JSON conversion
 *
 * @author Atiq
 */
public class JsonUtil {

    private static final String TAG = JsonUtil.class.getName();

    /**
     * This Method Convert Object in JaksonJSON String
     *
     * @param object
     * @return Json String or null if some error
     */
    public static String toJson(Object object) {
        try {
            Gson gson = new Gson();
            return gson.toJson(object);
        } catch (Exception e) {
            Log.e(TAG, "Error in Converting Model to Json", e);
        }
        return null;
    }

    /**
     * This method convert json to model
     *
     * @param json // * @param object
     * @return Model or null if some error
     */
    public static <T> Object toModel(String json, Type listType) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(json, listType);
        } catch (Exception e) {
            Log.e(TAG, "Error in Converting JSON to Model", e);
        }
        return null;
    }

    /**
     * This method map response to model 1.Get response entity 2.Convert to
     * string 3.Convert Josn String to Model
     * @param response : response which need to map
     * @param type : model type in which response is mapped
     * @return :model or null if some error
     */
    /*public static <T> Object mapResponseToModel(CZResponse response,
                                                Type type) {
		if (response != null
				&& response.getResponseCode() == 200
				&& type != null) {
			String resJson = StreamUtility.getJSONStringFromResponse(response);
			return JsonUtil.toModel(resJson, type);
		}
		return null;
	}*/

    /**
     * This Method map Json Data to listType
     */
    public static Object mapJsonToModel(Type listType, String jsonData) {
        Gson gson = new Gson();
        return gson.fromJson(jsonData, listType);
    }

    public static String getAssetJsonData(Context context, String fileName) {
        String json = null;
        try {
            // InputStream is = context.getAssets().open("myJson.json");
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        Log.e("data", json);
        return json;

    }

}
