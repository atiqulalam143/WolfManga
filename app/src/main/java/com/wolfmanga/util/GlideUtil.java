package com.wolfmanga.util;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.wolfmanga.R;


import java.io.File;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by Atiq on 24/9/16.
 */


public class GlideUtil {



   /*  *
     * @param context
     * @param imageView
     * @param url*/


    public static void loadImageCirculer(final Context context, final ImageView imageView, String url, int drawable) {

        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(drawable)
                        .error(drawable)
                        .dontAnimate()
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE))
                        .centerCrop())
                        .listener(new RequestListener<Drawable>() {
                               @Override
                               public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                                   return false;
                               }

                               @Override
                               public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                   return false;
                               }
                           })
                .into(imageView);
    }



    /***
     *
     * load image with default image
     **/


    public static void loadImageRactangle(final Context context, final ImageView imageView,
                                                 String url, int defaultImage) {
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .dontAnimate()
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(defaultImage)
                .placeholder(defaultImage))
                .into(imageView);


    }

    public static void loadImageRactangle(final Context context, final ImageView imageView,
                                          Uri url, int defaultImage) {
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .dontAnimate()
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(defaultImage)
                        .placeholder(defaultImage))
                .into(imageView);


    }

    public static void loadLookingForImage(final Context context, final ImageView imageView,
                                          String url, int defaultImage) {
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .dontAnimate()
                        .fitCenter()
                        .error(defaultImage))
                .transition(withCrossFade())
                .into(imageView);


    }

    public static void loadImageRoundedRactangle(final Context context, final ImageView imageView,
                                          String url, int defaultImage) {
        MultiTransformation multi = new MultiTransformation(
                new CenterCrop(),
                new RoundedCornersTransformation(20, 0, RoundedCornersTransformation.CornerType.ALL));


        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .dontAnimate()
                        .transform(multi)
                        .error(defaultImage)
                        .placeholder(defaultImage))
                .into(imageView);


    }

    public static void loadImageWithBlur(final Context context, final ImageView imageView,
                                         String url, int defaultImage){

        MultiTransformation multi = new MultiTransformation(
              //  new BlurTransformation(25),
                new CenterCrop(),
                new RoundedCornersTransformation(20, 0, RoundedCornersTransformation.CornerType.ALL));

        Glide.with(context).load(url)
                .apply(bitmapTransform(multi)
                        .transform(multi)
                .placeholder(defaultImage))
                .into(imageView);
    }


    public static void loadSvgImage(Context mContext,ImageView imageView,String url){
        RequestBuilder<PictureDrawable> requestBuilder= Glide.with(mContext)
                .as(PictureDrawable.class)
                .apply(new RequestOptions()
                        .dontAnimate()
                        .fitCenter()
                        .error(R.drawable.placeholder))
                .transition(withCrossFade());
        Uri uri = Uri.parse(url);
        requestBuilder.load(uri).into(imageView);
    }

    public static void loadImageRectangleFitXy(final Context context, final ImageView imageView,
                                               String url, int defaultImage) {
        ColorDrawable cd = new ColorDrawable(ContextCompat.getColor(context, R.color.colorPrimary));
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(defaultImage)
                        .dontAnimate()
                        .dontTransform()
                        .centerCrop()
                        .error(defaultImage))
                //.transition(withCrossFade())
                .into(imageView);


    }


    public static void loadImageWithPlaceholder(Context context, ImageView imageView,String url,int drwable) {
        ColorDrawable cd = new ColorDrawable(ContextCompat.getColor(context, android.R.color.darker_gray));
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(drwable)
                        .centerCrop())
                .transition(withCrossFade())
                .into(imageView);
    }

    public static void loadProfileIcon(String url, ImageView imageView,Drawable drawable) {
        Context context = imageView.getContext();
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(drawable)
                        .dontAnimate()
                        .fitCenter())
                .into(imageView);
    }




    /**
     * @param context
     * @param imageView
     * @param url
     */



    public static void loadImageWithThumbnail(final Context context, final ImageView imageView, String url,Drawable drawable) {
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(drawable)
                        .dontAnimate()
                        .fitCenter()
                        .circleCrop())
                .thumbnail(0.1f)
                .into(imageView);
    }
    public static void loadImageWithColor(final Context context, final ImageView imageView,
                                               String url, int drawable,int color) {

        Glide.with(context)
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(drawable)
                        .dontAnimate()
                        .error(drawable)
                        .fitCenter())
                //.thumbnail(0.1f)
                .into(imageView);
       // DrawableCompat.setTint(imageView.getDrawable(), color);
    }

    public static void loadDrawableImage(Context context, final ImageView imageView,
                                           int drawable) {

        Glide.with(context)
                .load(drawable)
                .apply(new RequestOptions()
                        .placeholder(drawable)
                        .dontAnimate()
                        .error(drawable)
                        .fitCenter())
               // .thumbnail(0.1f)
                .into(imageView);
      //  DrawableCompat.setTint(imageView.getDrawable(), color);
    }




    public static void loadImageFromUriWithDefaultImage(final Context context, final ImageView ivProfile,
                                                        Uri output, int drawable) {
        Glide.with(context)
                .load(new File(output.getPath()))
                .apply(new RequestOptions()
                        .placeholder(drawable)
                        .dontAnimate()
                        .fitCenter())
                .thumbnail(0.1f)
                .into(ivProfile);

    }


}
