package com.wolfmanga.catagory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.wolfmanga.R;
import com.wolfmanga.model.Widget;
import com.wolfmanga.model.WolfManga;

import java.util.ArrayList;

public class CatagoryListActivity extends AppCompatActivity implements CatagoryView{
    private RecyclerView recyclerView;
    private MangaAdapter adapter;
    private CategoryPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.rv);

        presenter=new CategoryPresenter(this);

        adapter = new MangaAdapter(new ArrayList<Widget>(),getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        presenter.loadCatagoryList();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager)recyclerView.getLayoutManager());
                int lastVisiblePosition = layoutManager.findLastVisibleItemPosition();
                if (lastVisiblePosition== adapter.getItemCount()-1 && dy>0) {
                    presenter.loadCatagoryList();
                }
            }

        });


    }

    @Override
    public void onLoadSuccess(WolfManga wolfManga) {
        adapter.setList(wolfManga.getWidgets());
    }
}
