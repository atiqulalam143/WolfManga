package com.wolfmanga.catagory;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wolfmanga.R;
import com.wolfmanga.model.Widget;
import com.wolfmanga.products.ProductAdapter;

import java.util.List;

public class MangaAdapter extends RecyclerView.Adapter<MangaAdapter.MangaViewHolder> {

    private List<Widget> widgetList;
    private Context context;

    public void setList(List<Widget> list) {
        this.widgetList.addAll(list);
        notifyDataSetChanged();
    }

    public class MangaViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public  RecyclerView rvProduct;

        public MangaViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txt_header);
            rvProduct = (RecyclerView) view.findViewById(R.id.rv_products);
        }
    }


    public MangaAdapter(List<Widget> widgetList, Context context) {
        this.widgetList = widgetList;
        this.context = context;
    }

    @Override
    public MangaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.manga_layout, parent, false);

        return new MangaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MangaViewHolder holder, int position) {
        Widget widget = widgetList.get(position);
        holder.title.setText(widget.getTitle());

        ProductAdapter adapter = new ProductAdapter(widget.getItems(),context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.rvProduct.setLayoutManager(mLayoutManager);
        holder.rvProduct.setItemAnimator(new DefaultItemAnimator());
        holder.rvProduct.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return widgetList.size();
    }
}
