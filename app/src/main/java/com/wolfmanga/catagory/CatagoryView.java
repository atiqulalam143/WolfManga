package com.wolfmanga.catagory;

import com.wolfmanga.model.WolfManga;

public interface CatagoryView {
    void onLoadSuccess(WolfManga wolfManga);
}
