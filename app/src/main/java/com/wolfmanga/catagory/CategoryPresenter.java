package com.wolfmanga.catagory;

import com.wolfmanga.WolfMangaApp;
import com.wolfmanga.model.WolfManga;
import com.wolfmanga.util.JsonUtil;

public class CategoryPresenter {
    private CatagoryView view;


    public CategoryPresenter(CatagoryView view) {
        this.view = view;
    }


    public void loadCatagoryList(){
        String jsonData= JsonUtil.getAssetJsonData(WolfMangaApp.getInstance(),"first.json");
        WolfManga wolfManga= (WolfManga) JsonUtil.toModel(jsonData,WolfManga.class);

        view.onLoadSuccess(wolfManga);
    }

}
