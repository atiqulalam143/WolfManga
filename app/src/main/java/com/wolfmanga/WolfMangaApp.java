package com.wolfmanga;

import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.util.Log;

import com.bumptech.glide.Glide;


/**
 * Created by atiq on 09/11/17.
 */

public class WolfMangaApp extends Application {
    private static WolfMangaApp context;

    public static WolfMangaApp getInstance() {
        if (context == null) {
            context = new WolfMangaApp();
        }
        return context;
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if(level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN){
            Log.d("App: ", "in background");
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.get(this).clearMemory();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;


    }


}
